/*
An ExtOR shim for extracting the USERADDR from incoming Moat connections
*/

package main

import (
	"flag"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"time"
)

type MoatHandler struct {
	ProxyPass string
}

// ServeHTTP will pass incoming HTTP requests to the ProxyPass address
// with an additional X-Forwarded-For header set to the UserAddr
// parsed from the ExtORPort. The response body and headers will be passed
// back to the requester.
func (h *MoatHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var err error

	transport := http.DefaultTransport.(*http.Transport)
	transport.ResponseHeaderTimeout = 15 * time.Second

	r.URL, err = url.Parse(h.ProxyPass + r.URL.Path)
	if err != nil {
		log.Fatalf("Error parsing proxy URL: %s", err.Error())
	}

	r.Header.Set("X-Forwarded-For", r.RemoteAddr)
	resp, err := transport.RoundTrip(r)
	if err != nil {
		log.Println("Error: %s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if resp.StatusCode != 200 {
		log.Printf("Error: status code %d", resp.StatusCode)
		w.WriteHeader(resp.StatusCode)
		return
	}
	//Pass along all of the appropriate headers
	for header, _ := range resp.Header {
		w.Header().Set(header, resp.Header.Get(header))
	}

	//Write the response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Error: %s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	resp.Body.Close()
	w.Write(body)
}

func main() {
	var extOrAddr string
	var proxyPass string
	var certFile string
	var keyFile string
	var authCookieFile string

	flag.StringVar(&extOrAddr, "addr", "127.0.0.1:5000",
		"port to listen for incoming OR connections")
	flag.StringVar(&proxyPass, "proxy", "http://127.0.0.1:80",
		"destination for incoming HTTP requests")
	flag.StringVar(&certFile, "cert", "", "TLS certificate file")
	flag.StringVar(&keyFile, "key", "", "TLS private key file")
	flag.StringVar(&authCookieFile, "auth", "",
		"TOR_PT_AUTH_COOKIE_FILE for ExtOR connections")
	flag.Parse()

	addr, err := net.ResolveTCPAddr("tcp", extOrAddr)
	if err != nil {
		log.Fatalf("Could not resolve ExtOrAddr: %s", err.Error())
	}

	var ln net.Listener
	if authCookieFile != "" {
		var authCookie []byte
		var inerr error
		// If the authCookie file already exists, don't overwrite it because the
		// meek server might already be using it
		if _, err := os.Stat(authCookieFile); err == nil {
			authCookie, inerr = readAuthCookieFile(authCookieFile)
			if inerr != nil {
				log.Fatalf("Error reading from auth cookie file: %s", inerr.Error())
			}

		} else if os.IsNotExist(err) {
			authCookie, inerr = writeAuthCookieFile(authCookieFile)
			if err != nil {
				log.Fatalf("Error writing to auth cookie file: %s", inerr.Error())
			}

		} else {
			log.Fatalf("Error handling auth cookie file: %s", err.Error())
		}
		ln, err = ListenExtOr("tcp", addr, authCookie)
	} else {
		ln, err = net.ListenTCP("tcp", addr)
	}
	if err != nil {
		log.Fatalf("Error listening on ExtOrAddr: %s", err.Error())
	}

	handler := &MoatHandler{
		ProxyPass: proxyPass,
	}

	if certFile == "" || keyFile == "" {
		err = http.Serve(ln, handler)
		if err != nil {
			log.Fatalf("Error, closing TLS server: %s", err.Error())
		}
		log.Fatalf("You must provide a path to valid TLS cert and key files.")
	} else {
		err = http.ServeTLS(ln, handler, certFile, keyFile)
		if err != nil {
			log.Fatalf("Error, closing TLS server: %s", err.Error())
		}
	}

	log.Println("Exiting.")

}
